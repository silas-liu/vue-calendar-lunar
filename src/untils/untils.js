function formatDate (ev) {
  // state true 只要年月 ,false全部包括时分秒
  const dateTime = new Date(ev)
  const year = dateTime.getFullYear()
  const month = dateTime.getMonth() + 1
  const date = dateTime.getDate()
  // const hour = dateTime.getHours()
  // const minute = dateTime.getMinutes()
  // const second = dateTime.getSeconds()
  return `${year}-${addZero(month)}-${addZero(date)}`
  /*     return state ? `${year}-${addZero(month)}` : `${year}-${addZero(month)}-${addZero(date)}  ${addZero(hour)}：${addZero(minute)}：${addZero(second)}`  */
}

function addZero (v) {
  return v < 10 ? '0' + v : v
}

export default formatDate
