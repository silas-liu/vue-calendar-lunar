import { createPlugin } from '@fullcalendar/core'

const CustomViewConfig = {

  classNames: ['custom-view'],

  content: function (props) {
    // let segs = sliceEvents(props, true) // allDay=true
    let html =
      // '<div class="view-title">' +
      // props.dateProfile.currentRange.start.toUTCString() +
      // '</div>'
      '<div class="view-title">' +
      '<div class="mon">' + '01月' + '</div>' +
      '<div class="mon">' + '02月' + '</div>' +
      '<div class="mon">' + '03月' + '</div>' +
      '<div class="mon">' + '04月' + '</div>' +
      '<div class="mon">' + '05月' + '</div>' +
      '<div class="mon">' + '06月' + '</div>' +
      '<div class="mon">' + '07月' + '</div>' +
      '<div class="mon">' + '08月' + '</div>' +
      '<div class="mon">' + '09月' + '</div>' +
      '<div class="mon">' + '10月' + '</div>' +
      '<div class="mon">' + '11月' + '</div>' +
      '<div class="mon">' + '12月' + '</div>' +
      '</div>'
    return { html: html }
  }

}

export default createPlugin({
  views: {
    custom: CustomViewConfig
  }
})
