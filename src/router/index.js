import Vue from 'vue'
import Router from 'vue-router'
// 主页面结构
const Calendar = () => import('@/views/calendar.vue')
// 说明
Vue.use(Router)
const routes = [{
  path: '*',
  redirect: '/ca'
},
{
  path: '/ca',
  name: 'Calendar',
  component: Calendar
}
]

const router = new Router({
  // mode:'history',//default: hash ,history
  // base: '/vue-lunar-fullcalendar',
  routes,
  linkActiveClass: 'my-active',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: 0
      }
    }
  }
})
export default router
