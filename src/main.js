// 重置样式
import '@/assets/css/reset.scss'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import Vue from 'vue'
import App from './App'
import router from './router'
Vue.config.productionTip = false

Vue.use(Antd)
// import lunarFullCalendar from '../lib/LunarFullCalendar.min.js'
// Vue.use(lunarFullCalendar)

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: (h) => h(App)
})
